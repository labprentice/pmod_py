# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 11:48:38 2016

@author: GreencyclesII
"""
# NOAA co2 monthly mean data comes as a text file. This will convert the text file into a csv file with headers:
#  year month     decimal     average       trend

import csv

co2_read = [] 

# Create a list of values and remove the comments form the top
with open('/Users/GreencyclesII/Documents/Rebecca Thomas/PhD/Part2_MODEL/Code_MODEL/data/noaa/co2_mm_gl.txt','r') as f:
    lines_read = f.readlines()
        
    for line in lines_read:
        li = line.strip()
        if not li.startswith("#"):
            co2_read.append(li)
            
f.close()

# Create a CSV file and write to co2_monthly.txt
with open('co2_monthly.txt', 'wb') as myfile:
    wr = csv.writer(myfile)
    wr.writerow(co2_read)
#    for line in co2_read:
#         wr.writerow(line.split())
