# -*- coding: utf-8 -*-
"""
Created on Mon Aug  1 15:27:22 2016

@author: rebecca
"""

from scipy.io import netcdf
from matplotlib.dates import date2num
from dateutil.relativedelta import relativedelta
import logging

import cPickle as pickle
from datetime import date
import time
import numpy as np


class make_netCDF:
    """"
    Name: make_netCDF
    """
    
    def __init__(self):
        """
        Name: make_netCDF.__init__
        Input: None
        Features: Initialsie class variables
        """
        self.logger = logging.getLogger(__name__)
        self.logger.debug("Saving data to netCDF")
        ### Class Functions Definitions ###
        
    def make_the_file(self, var2write, data2write, varName, lats, longs, start_date, varUnit, p_type, phi_0 = None, beta = None, greening = None):
        """
        Name: make_netCDF.make_the_file
        Input:  var2write - variable to write
                lats - array of altitdes
                longs - array of longitudes
                start_date - time
        """
        self.logger.info("Writing %s to a netCDF file" %var2write)
        fname = var2write + '.nc'
   
        f = netcdf.netcdf_file(fname,'w')
        
        no_lats = lats.shape[0]
        no_longs = longs.shape[0]
        
        no_dims2write = np.ndim(data2write)
        print no_dims2write
        
    
        self.logger.info("Creating dimensions")
        if no_dims2write == 3 :
             f.createDimension('time', None)
             print 'Created time'
        f.createDimension('longitude', no_longs)
        f.createDimension('latitude', no_lats)
        
        
        
        latitudes = f.createVariable('latitude', 'float32', ('latitude',))
        longitudes = f.createVariable('longitude', 'float32', ('longitude',))
        if no_dims2write == 3 :
            times = f.createVariable('time', 'f', ('time',))
            print 'Created main'
            main = f.createVariable('%s' %varName, 'f' ,('time','latitude','longitude'))
            times.units = 'days since 0001-01-01 00:00:00'
            times.calendar = 'gregorian'
        
        elif no_dims2write == 2 :
            main = f.createVariable('%s' %varName, 'f' ,('latitude','longitude'))
       
        
        latitudes[:] = lats   
        longitudes[:] = longs
        
        latitudes.units = 'degree_north'
        longitudes.units = 'degree_east'
        
        
        if no_dims2write == 3:
            main[:, :, :] = data2write 
            print 'Data written'
        elif no_dims2write == 2:
            main[:,:] = data2write
            desc_var = ('%s for %s using the regridding script from X Gilbert' %(varName, str(start_date )))
            main.description = desc_var


        main.units = varUnit 
        main._FillValue = 9.96920996839e+36
        main.source = 'NetCDF file written by Rebecca Thomas'
        main.history = 'Created ' + time.ctime(time.time()) 
        
        if phi_0:
            descr_var = ('%s created with phi 0 = %s, beta = %s, and greening data from %s. Photosynthetic pathway: %s' %(var2write, phi_0, beta, greening, p_type))
            main.description = descr_var
    
        if no_dims2write == 3:
            print 'Created dates'
            dates = []
            saving_start = start_date + relativedelta(days = 1)
            print saving_start
            
            
            if 'daily_out' in varName:
                # This comes out daily so save days rather than months
                noDays = data2write.shape[0]
                print 'Days:', noDays
                d = 0
                m = 0
                y = 0
                
                for n in range(1,noDays + 1):
                    print ('Y: %f M: %f D: %f' %(y, m, d))
               
                    if d == 365: # and y != 0:
                        y += 1
                        m = 1
                        d = 1
                      
                    else:
                        m += 1
                        d += 1
                        
                    dates.append (saving_start + relativedelta(day = d) + relativedelta(month = m) + relativedelta(years = y))       
             
            else:   
            
                noMonths = data2write.shape[0]
                print noMonths
              
                m = 0
                y = 0
                    
                for n in range(1,noMonths + 1):
                    print ('Y: %f M: %f ' %(y, m))
                   
                    if m == 12: # and y != 0:
                        y += 1
                        m = 1
                        
                      
                    else:
                        m += 1
                        
                    dates.append (saving_start + relativedelta(month = m) + relativedelta(years = y))       
                
#        elif no_dims2write == 2:
#              dates = start_date
              
            times[:] = date2num(dates)
              
        f.close()    
        
        self.logger.info("%s written to netCDF" %var2write)
        
    ######### MAIN PROGRAM #######
    
if __name__ == '__main__':
    
    var2write = 'GPP'
    
    with open('/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/output/splash4cesar/alpha_2011_run_5_7.pickle', 'rb') as input_file:
        all_var = pickle.load(input_file)
#    gpp_out = all_var.get('GPP')
    
    var2write = 'Alpha_2011_2012'
    just_alpha = all_var[0:24,:,:]
    data2write = just_alpha
    
    lat_in = np.linspace(-90, 89.5, 360)
    lon_in = np.linspace(-180., 179.5, 720)
    
    start_date = date(2011, 1, 1)
    units = 'none'
    phi_0 = 'n/a' #0.83
    beta = 'n/a' #146.0
    greening = 'n/a' #'fAPAR'
    
    
    run_make_netcdf = make_netCDF()
    run_make_netcdf.make_the_file(var2write, data2write, 'Alpha',lat_in, lon_in, start_date, 'NA', units, phi_0, beta, greening )  
