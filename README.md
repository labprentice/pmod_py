# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

The is the Python implementation of the global and site-scale P-model. The equations used for this are detailed below. Please make sure you branch off the master when you are using the code.

* Version
V 1.0 (?)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up 

[Global simulations]
In order to run this version of the P-model, you need to also pull the code from the repository: SPLASH/working/py_version. You need daily precipitation and temperature, and monthly sunshine fraction to run the SPLASH module, and monthly fAPAR (or other greening data), mean monthly air temperature, maximum monthly temperature, minimum monthly air temperature, monthly vapour pressure, monthly ambient CO2 concentration and elevation. 

You need to take the following steps:

1. Set up the folders (just the first level of the directory highlighted with **) for the SPLASH code, input data and the Pmodel code in the following way (Note: The src folder is the one with all the P-model code in i.e. this repositroy):

![Screen Shot 2017-01-18 at 11.44.49.png](https://bitbucket.org/repo/Rboz5R/images/840572096-Screen%20Shot%202017-01-18%20at%2011.44.49.png)

2. Go into your newly created "src" repository and clone the code. Then branch off this, so you can make local changes which can then be merged to the main branch if applicable:

```
#!bash

git clone https://RT5@bitbucket.org/labprentice/pmod_py.git .
git checkout -b <your_branch_name>
```

Make sure to set the upstream of your branch so you can commit your changes to a branch here on the online bitbucket repository. Everyone will be able to see your code here which makes it easy to share and check code :)
```
#!bash

git push --set-upstream origin <your_branch_name>
```

Note: If changes have been made to the master branch e.g. because we have decided to incorporate something new into the model, you can update your local branch to include these changes using the command:
```
#!bash

git fetch origin
git merge origin/master
```

3. Populate the cru, noaa, fapar and watch_wfdei folders with appropriate data (e.g. by creating symbolic links on CX1)

4. Run the code! Use the script start_model.py (I would recommend changing this for each run configuration e.g. start_model_test_run.py etc.)

5. The output is a netCDF3 file of monthly GPP. You can select to save additional output y toggling the appropriate variables to True (Haven't really tested this).


[Site-level simulations]

1. In order to run the site-level simulations, you will first need to follow Beni's instructions for creating the input data for the FLUXNET sites. 

2. This is where you populate the input_fluxnet_sofun/sitedata folder with input data. The structure shown in the picture above should be how the output data is saved when you create it, so it should jsut be a question of copying it into the folder.
 
3. Now put the siteparamfiles into the sitepatamfils folder. Again, this should simply involve copying the run repository (created when running Beni's code) - containing files with the format <site_name>.sofun.parameter, and the site_paramfils repository - containing files with the format <site_name>.parameter.

4. The site-level simulation runs in almost the same way as the global simulations. Choose "site_level" in the start_model.py script, and this *should* run for each site for which you have input data (This has only been tested with one site so far, so there might be some bugs here). 

5. Output is saved as text files in the output folder. You do not need to create folders for each site in this folder, these will be created automatically if they do not currently exist. Output shouldn't be overwritten if you change the simulation name for each simulation run you do. 


* Equations (I don't know why these pictures come out so big!)

![GPP_equation_picture.jpg](https://bitbucket.org/repo/Rboz5R/images/1180007373-GPP_equation_picture.jpg)

![m_equaiton_picture.jpg](https://bitbucket.org/repo/Rboz5R/images/2439964985-m_equaiton_picture.jpg)


* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Me - Rebecca! r.thomas14@imperial.ac.uk
* Other community or team contact