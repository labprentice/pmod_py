# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 10:16:38 2016

@author: GreencyclesII
"""



gamma_25 = 4.220 # Pa  Gamma* at 25C
ko25 = 27480 # Pa, at 25 deg C and 98.716KPa
kc25 = 39.97 # Pa at 25 deg C and 98.716KPa
es0 = 0.611 # saturation vapour pressure constant

kelv2cel = 273.15 # add this to celcius to get Kelvin
t_25 = 298.15 # K 25C in Kelvin
Ha = 37830.0 #J/mol Activiation energy for Gamma*
Ha_kc = 79430 #J/mol Activation energy for Kc
Ha_ko = 36380 # J/mol Activation energy for Ko
R = 8.3145 # J/mol/K Universal gas constant
eta_const = -0.0227 # No unit  Constant for viscosity of water relative to its value at 25C (See Wang 2015)
kco =  2.09476e5 #ppm. US standard pressure. (From Beni's code - Ref Bernacchi et al 2001)
kPo = 101325.0  # Standard atmopsheric pressure (Pa), Allen 1973

Ho = 36.38 # Bernacchi 2001 energy of activation for oxygenation
Hc = 79.43 # Bernacchi 2001 energy of activation for carboxylation

c_star = 0.41 # unit of carbon cost for maintenance of electron transport capacity (obs Jmax:Vc max)
a_hat = 4.4 # Standard value for c13 discrimination - diffusion component (Wang 2015 Eq s42)
b_hat = 27.0 # Standard value for c13 discrimination - biochemical component (Wang 2015 Eq s42)

c_molmass =  12.0107  # g C / mol C

C3_phi0 = 0.092 * c_molmass # # mol/mol Long et al. 1993 # gC/mol Intrinisic quantum yield of photosynthesis for C3 plants
C4_phi0 = 0.055 * c_molmass # # mol/mol Long et al. 1993 # gC/mol Intrinisic quantum yield of photosynthesis for C4 plants
