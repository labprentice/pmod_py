# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 15:04:15 2016

@author: GreencyclesII
"""
#Testing Git
from gpp_calc import GPP_CALC
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import scipy.io

#import timing
from datetime import timedelta, date, datetime
from scipy.io import netcdf
import time
#from scipy.stats import nanmean
import cPickle as pickle
from dateutil.relativedelta import relativedelta
import logging
#from nco import Nco
#nco = Nco()
import subprocess

CURRENT_DIR = os.path.abspath(os.curdir)
PARENT_DIR = os.path.abspath(CURRENT_DIR + "/../") 
GRANDPARENT_DIR = os.path.abspath(CURRENT_DIR + "/../../") 
        
splash_path = os.path.join(PARENT_DIR, 'SPLASH/working/py_version') # the SPLASH code location
sys.path.append(splash_path)       

 

from splash import SPLASH
from solar import SOLAR
from data_grid import DATA_G
from run_site import RUN_SITE

class RUN_P_MODEL:
    """
    Name: RUN_P_MODEL
    Features: The full P_model
    """
    def __init__(self, simName, run_area, p_type, start_date = None, end_date = None ):
        """
        Name: RUN_P_MODEL.__init__
        Input: - date, start date of run  
               - date, end date of run 
        """
        # Create a class logger
        self.logger = logging.getLogger(__name__)
        self.logger.info("Running model from %s to %s for %s" % (str(start_date), str(end_date), simName))
        
        # Initialise DATA class
        try:
            self.data = DATA_G()
        except:
            self.logger.exception("Failed to initialise DATA class")
        else:
            self.logger.debug("Initialised DATA class")

        #set simulation name for saving             
        self.sim = simName
        
       # What type of photosynthesis are we dealing with? Don't need to do aything if its C3, but need to change the CO2 input if its C4.
        self.ptype = p_type 
        
         #Initilaise the output files 
    
    
        if run_area == 'global':        
            self.run_range = relativedelta(end_date  + timedelta(1), start_date)
            noMonths =  (self.run_range.years * 12) + self.run_range.months   


            self.GPP_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.LUE_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.C13_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.NETRAD_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.PRECIP_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.green_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            
            
            self.ci_ca_ratio = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.alpha = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.sum_month_aet = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.sum_month_eet = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.sum_month_precip = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.sum_month_netrad = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.PAR_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.tmin_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.tmax_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.temp_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.vap_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            self.aCO2_out = None # np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
            
            self.wn = None #np.empty( shape = (self.no_lats, self.no_lons))
        
          
       
    def find_driving_data(self, green_driver = None,
                       daily_splash_driver = None, 
                       monthly_gpp_driver = None, 
                       annual_co2_driver = None):
        
        
        self.logger.info("Retrieving driver data")
       
        if monthly_gpp_driver:
            self.cru_dir =os.path.join(PARENT_DIR + '/input/' + monthly_gpp_driver ) 
            self.logger.info("MONTHLY data found. Using ... %s" %(self.cru_dir))
            self.data.find_cru_files(self.cru_dir)
        else:
            self.cru_dir = []
            self.logger.info("no monthly data source defined")
            
        if daily_splash_driver:    
            self.watch_dir = os.path.join(PARENT_DIR + '/input/' + daily_splash_driver)
            self.logger.info("DAILY data found. Using ... %s" % (self.watch_dir))
            
           
        else:
            self.watch_dir = []
            self.logger.info("No daily data source defined")
        
        if annual_co2_driver:
            self.co2_dir = os.path.join(PARENT_DIR + '/input/' + annual_co2_driver)
            self.logger.info("CO2 data found. Using ... %s" % (self.co2_dir))
            self.glob_co2 = self.data.find_noaa_files(self.co2_dir)
        else: 
            self.co2_dir = []
            self.logger.info("No co2 data defined")
       
        if green_driver == 'fAPAR':    # Check if fapar directory exists (i.e. using fAPAR)
            self.logger.info ("Using fAPAR3g for greening data")
            self.fapar_dir =os.path.join(PARENT_DIR + '/input/' + green_driver  ) 
            self.logger.info("Greening data found. Using ... %s" % (self.fapar_dir))
            self.data.find_fapar_files(self.fapar_dir)
        elif green_driver == 'evi':
            self.logger.info("Using EVI for greening data")
            self.evi_dir =os.path.join(PARENT_DIR + '/input/' + green_driver  ) 
            self.logger.info("Greening data found. Using ... %s" % (self.evi_dir))
        else:
            self.grid_green = []
            self.logger.info("No greening data defined")
        
        self.data.read_elv()    
        self.data.read_lon_lat()
   
    def run_spin_up_gridded(self, start_date, drivers, wn = np.tile(145.0, (360,720))):
        
        
        
        # Choose date range of spin up - defualt is 1 year
        start_spin_up = start_date - relativedelta(years = 1)
        #print start_spin_up
        end_spin_up = start_date #+ relativedelta(days = 1)
        #print end_spin_up
        self.logger.info("Running spin-up from %s to %s " % (str(start_spin_up), str(end_spin_up)))
  
  
        for my_date in self.daterange_daily(start_spin_up, end_spin_up):    
            
            yr = my_date.year
           
            doy = my_date.timetuple().tm_yday

            if my_date.month == 2 and my_date.day == 29:
                    leap_day = True
            else:
                    leap_day = False
                     
            self.logger.info("Running for DOY %s" %str(doy))
            #Find driving data
            self.find_driving_data(daily_splash_driver = drivers['daily_splash_driver'], 
                                   monthly_gpp_driver = drivers['monthly_gpp_driver'])
            watch_dir = self.watch_dir            
            
            
            # Get the driving data
            self.data.find_watch_files(watch_dir, my_date)
            self.data.read_daily_clim(my_date)
            grid_elv = self.data.elevation
            
            grid_sf = self.data.sf
            grid_tair = self.data.Tair 
            grid_rainf = self.data.Rainf * 1.08
            
            #Run the splash model to spin-up soil moisture
            for i in range(len(self.data.latitude)):
                         
                 grid_lat =self. data.latitude[i]
                
                 self.make_splash = SPLASH(grid_lat, grid_elv[i, :])
                 self.make_splash.run_one_day(doy, yr, wn [i,:], grid_sf [i, :], grid_tair[i, :], grid_rainf[i, :], leap_day)
                 self.wn[i, :] = self.make_splash.wn
                     
            self.logger.debug("calculated global soil moisture for %s" %str(my_date))   
         
    
             
        
    def run_full_mod(self, start_date, end_date, drivers, od, beta, phi_0, absG, yr_drive, wn = None):
        
        self.logger.info("Running full P-model")
     

# Comment this out once you've got everything working   
        tempSave = {}

# Comment this out if you're not using 0.5x0.5 global grid. Need to delete this and make new code to get m2 to degrees
        mat_grid = scipy.io.loadmat('../areaGridHalfDegRes.mat')
        area_grid = mat_grid['areaGridHalfDegRes'] 

        #Find driving data
        self.find_driving_data(green_driver = drivers['green_driver'],
                                daily_splash_driver = drivers['daily_splash_driver'], 
                                monthly_gpp_driver = drivers['monthly_gpp_driver'],
                                annual_co2_driver = drivers['annual_co2_driver'])

        grid_size = self.data.elevation
        self.no_lats = grid_size.shape[0]
        self.no_lons = grid_size.shape[1]
        
        # Initialise all output vars
        self.run_range = relativedelta(end_date  + timedelta(1), start_date)
        noMonths =  (self.run_range.years * 12) + self.run_range.months  

        self.GPP_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.LUE_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.C13_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.PRECIP_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.ci_ca_ratio = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.alpha = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.PAR_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.tmin_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.tmax_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.temp_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.vap_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        self.aCO2_out = np.zeros(shape = (noMonths, self.no_lats, self.no_lons))
        
        # Splash vars
        self.sum_month_aet = np.zeros(shape = (self.no_lats, self.no_lons))
        self.sum_month_eet = np.zeros(shape = (self.no_lats, self.no_lons))
        self.sum_month_precip = np.zeros(shape = (self.no_lats, self.no_lons))
        
        
        month_ppfd = np.empty(shape = (31, self.no_lats, self.no_lons))
        month_ppfd.fill(np.nan)
        
        
        if wn:
            month_aet = np.empty(shape = (31, self.no_lats, self.no_lons))
            month_aet.fill(np.nan)
            month_eet = np.empty(shape = (31, self.no_lats, self.no_lons))
            month_eet.fill(np.nan)
            month_precip = np.empty(shape = (31, self.no_lats, self.no_lons))
            month_precip.fill(np.nan)
            month_netrad = np.empty(shape = (31, self.no_lats, self.no_lons))
            month_netrad.fill(np.nan)

        
                           
       
        watch_dir = self.watch_dir 
        glob_co2 = self.glob_co2      
            
        monthNo = 0
        for my_date in self.daterange_daily(start_date, end_date + relativedelta(days = 1)):  
            
            
            tomorrow = my_date + timedelta(1)
            
            if yr_drive['splash_daily_yr'] == 'const':
                yr_run = start_date.year
                mo_run = my_date.month
                day_run = my_date.day
                if mo_run == 2 and day_run == 29:
                    day_run -=1
                    leap_day = True
                else:
                    leap_day = False
                sd_run_date = date(yr_run, mo_run, day_run)
            else:
                sd_run_date = my_date
                leap_day = False
#            print ("sd_date = %s" %str(sd_run_date))
             
            self.data.find_watch_files(watch_dir, sd_run_date)
            self.data.read_daily_clim(sd_run_date)
            grid_elv = self.data.elevation
            
            doy = sd_run_date.timetuple().tm_yday
            yr = sd_run_date.year
            
            month = sd_run_date.month
           
            grid_sf = self.data.sf
            grid_tair = self.data.Tair 
            
            if wn:
                grid_rainf = self.data.Rainf * 1.08
            
           
            
            for i in range(len(self.data.latitude)):
                         
                     grid_lat = self.data.latitude[i]

                     if wn: # Only run full splash if you want alpha
                         self.make_splash = SPLASH(grid_lat, grid_elv[i, :])
                         self.make_splash.run_one_day(doy, yr, wn [i,:], grid_sf [i, :], grid_tair[i, :], grid_rainf[i, :], leap_const = leap_day)
                         wn[i, :] = self.make_splash.wn
                     
             
                         month_aet[my_date.day - 1, i, :] = self.make_splash.aet
                         month_eet[my_date.day - 1, i, :] = self.make_splash.eet
                         month_precip[my_date.day - 1, i, :] = grid_rainf[i,:]

#                     if leap_day:
#                         month_ppfd[my_date.day - 1, i, :] = self.make_splash.ppfd_d * np.nan
#                     else:
    
                     else: # If you jsut want solar output, run this instead
                        self.make_splash = SOLAR(grid_lat, grid_elv[i, :])
                        self.make_splash.calculate_daily_fluxes(doy, yr, grid_sf [i, :], grid_tair[i, :])
                        
                     month_ppfd[my_date.day - 1, i, :] = self.make_splash.ppfd_d
                     
                    # month_netrad[my_date.day - 1, i, :] = self.make_splash.netrad
                     
            if month <> tomorrow.month or my_date == end_date:
                get_gpp = 1
            else:
                get_gpp = 0         
      
            if get_gpp == 1:
            
                if yr_drive['gpp_monthly_yr'] == 'const':
                    yr_run = start_date.year
                    mo_run = my_date.month
                    day_run = my_date.day
                    if mo_run == 2 and day_run == 29:
                        day_run -=1
                    gm_run_date = date(yr_run, mo_run, day_run)
                else:
                    gm_run_date = my_date
#                print ("gm_date = %s" %str(gm_run_date))  
                
                if yr_drive['green_yr'] == 'const':
                    yr_run = start_date.year
                    mo_run = my_date.month
                    day_run = my_date.day
                    if mo_run == 2 and day_run == 29:
                        day_run -=1
                    fm_run_date = date(yr_run, mo_run, day_run)
                else:
                    fm_run_date = my_date
                
                if wn:
                    self.sum_month_aet = np.nansum(month_aet,0)
                    month_aet.fill(np.nan)
                    self.sum_month_eet = np.nansum(month_eet, 0)
                    month_eet.fill(np.nan)
                    self.sum_month_precip = np.nansum(month_precip,0)
                    self.grid_alpha = self.sum_month_aet/self.sum_month_eet
                    self.grid_alpha[np.isinf(self.grid_alpha)] = np.nan
#                self.sum_month_netrad = np.nansum(month_netrad,0)
                else:
                    self.grid_alpha = np.tile( 1.0, (self.no_lats, self.no_lons))

                grid_ppfd = np.nansum(month_ppfd, 0)   
                
                if drivers['green_driver'] == 'evi':
                    self.data.find_evi_files(self.evi_dir, fm_run_date)
                    
                    
                self.data.read_monthly_clim(gm_run_date)
                self.data.read_monthly_fAPAR(fm_run_date)
                
                if drivers['green_driver'] == 'fAPAR':
                     self.grid_green = self.data.fAPAR
                elif drivers['green_driver'] == 'evi':
                     orig_evi = self.data.evi
                     self.grid_green = np.flipud(orig_evi)
                    
                    
                # asume uniform incerase in temp of 2.5C
                grid_tmp = self.data.tmp 
                grid_tMax = self.data.tmp
                grid_tMin = self.data.tmn 
                grid_vap = self.data.vap
                
                if self.ptype == 'C4':
                    grid_aCO2 = np.tile(9999.9, (self.no_lats, self.no_lons))
                else: # This is for C3 plants
                    if yr_drive['co2_yr'] == 'const':
                        yr_run = start_date.year
                        mo_run = my_date.month 
                        co2_run_date = date(yr_run, mo_run, 1)
                    else:
                        co2_run_date = my_date
                        
                        
                    co2_run_month = co2_run_date.month
                    co2_run_yr = co2_run_date.year
                    
                    # CO2 input is a sinlge global value - so need to tile this for global input
                    date_ind = round(float(co2_run_yr + (co2_run_month/12.0) - (0.083/2.0)),3)
                    month_co2_idx = np.where(glob_co2 == date_ind)
                    #grid_aCO2 = np.tile(glob_co2[month_co2_idx[0],3], (self.no_lats, self.no_lons)) 
                    grid_aCO2 = np.tile(540.0, (self.no_lats, self.no_lons))  # ppm 
                
#                print ( "Calculating GPP for: %d %d" %(my_date.month, my_date.year))
#                print ("CO2 = %s" %str(grid_aCO2[0][0]))sky
               
                self.logger.info( "Calculating GPP for: %d %d" %(my_date.month, my_date.year))
                self.logger.info("Using driving data for: %d %d" %(gm_run_date.month, gm_run_date.year))
                
                

                print("fm_date = %s" %str(fm_run_date))
                print("co2 = %s" %str(grid_aCO2[0,0]))
#                print("fapar_driver = %s" %str(self.grid_green[282,359]))
#                print("temp_driver = %s" %str(grid_tmp[282,359]))
#                print("aCO2_driver = %s" %str(grid_aCO2[282,359]))
#                print("alpha_driver = %s" %str(self.grid_alpha[282,359]))
#                print("elv_driver = %s" %str(grid_elv[282,359]))
#                print("tmax_driver = %s" %str(grid_tMax[282,359]))
#                print("tmin_driver = %s" %str(grid_tMin[282,359]))
#                print("vap_driver = %s" %str(grid_vap[282,359]))

            
                self.make_gpp = GPP_CALC()
                self.make_gpp.run_grid(grid_ppfd, self.grid_green, grid_tmp, grid_aCO2, self.grid_alpha, grid_elv, beta, phi_0, absG, grid_tMax, grid_tMin, grid_vap)
                
                self.GPP_out[monthNo, :, :] = self.make_gpp.gpp #  gC/m2/month
                self.LUE_out[monthNo, :, :] = self.make_gpp.lue
                self.PRECIP_out[monthNo, :, :] = self.sum_month_precip
                self.temp_out[monthNo, :, :] = grid_tmp
                self.aCO2_out[monthNo, :, :] = grid_aCO2
                self.alpha[monthNo, :, :] = self.grid_alpha
                self.temp_out[monthNo, :, :] = grid_tmp
                self.tmin_out[monthNo, :, :] = grid_tMin
                self.tmax_out[monthNo, :, :] = grid_tMax
                self.vap_out[monthNo, :, :] = grid_vap
                self.C13_out[monthNo, :,:] = self.make_gpp.c13
                self.ci_ca_ratio[monthNo,:,:] = self.make_gpp.ci_ca
                self.PAR_out[monthNo,:,:] = grid_ppfd

                self.logger.info( "GPP = %0.3f PgC/month" %(np.nansum(self.make_gpp.gpp * 1E-15 * area_grid)))
#                print("GPP = %0.3f gC/m2/month" %(np.squeeze(self.make_gpp.gpp[282,359])))
                
                monthNo += 1
   
                month_ppfd.fill(np.nan)
                
                 # This needs some work - currently jsut saving the file name e.g. self.gpp_out 
                        # save as you go along 
                
                
                tempSave['GPP'] = self.GPP_out
                tempSave['LUE'] = self.LUE_out
#                tempSave['PRECIP'] = self.PRECIP_out
#                tempSave['Green'] = self.green_out
                               
                
#                for ii in range(len(files2save[0])):
#                    varName = files2save[1][ii]
#                    varData = files2save[0][ii]
#      
                self.save2pickle('temp_output', tempSave, self.sim)
              
#                with open('gpp_fapar_1982_2011_run_18_08.pickle', 'wb') as out_file:
#                     pickle.dump(self.GPP, out_file, pickle.HIGHEST_PROTOCOL)
#                      
#                with open('c13_1982_2011_run_18_08.pickle', 'wb') as out_file:
#                      pickle.dump(self.C13_out, out_file, pickle.HIGHEST_PROTOCOL)     
#        
        self.gpp_PgC_yr = self.GPP_out * area_grid * 1E-15
        self.gpp_total = np.nansum(self.gpp_PgC_yr) / self.run_range.years  #run_range.years
        self.logger.info(("GPP =: %0.3f PgC/yr") % (self.gpp_total))
        print ("GPP =: %0.3f PgC/yr") % (self.gpp_total)
    
    def run_site_mod(self, beta, phi_0, absG, outr, site_list_loc, site_data_loc, param_data_loc):
        
    
        site_list_path = os.path.join(PARENT_DIR, site_list_loc)  
        print site_list_path
        
        site_data_path = os.path.join(PARENT_DIR, site_data_loc)
        
        print site_data_path
        param_data_path = os.path.join(PARENT_DIR, param_data_loc)
        print param_data_path
        self.logger.info("Getting site data")
        self.site_run = RUN_SITE()
        self.site_run.run_site(beta, phi_0, absG, outr, site_list_path, site_data_path, param_data_path) # Run the model for each site in a loop
        if outr.get('GPP'):
            self.GPP_out = self.site_run.site_gpp  
        if outr.get('LUE'):
            self.lue_out = self.site_run.site_lue
        if outr.get('TEMP'):
            self.temp_out = self.site_run.site_temp
       
        
        self.site_date = self.site_run.site_gpp_date
        
        
        
#      
    
#               self.find_site_data(first_year_trend, sitename)

   
#                yield flux_loc
   
    def read_my_lines(self,csv_reader, lines_list):
        for line_number, row in enumerate(csv_reader):
             print line_number
             print row
             if line_number in lines_list:
               
                yield line_number, row
    # from http://stackoverflow.com/questions/29567023/how-to-read-specific-lines-of-a-large-csv-file
     
    def daterange_daily(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)  
            
    def daterange_year(self, start_date, end_date):
            for n in range(int ((end_date - start_date).years)):
                yield start_date + timedelta(n)                   
            
    def save2pickle(self, varName, data2save, sim):
        
        var_sim = (varName + '_'+ sim)
        with open('%s.pickle' % var_sim, 'wb') as out_file:
            pickle.dump(data2save, out_file, pickle.HIGHEST_PROTOCOL)


###### MAIN PROGRAM #########

if __name__ == '__main__':
    
#    root_logger = logging.getLogger()
#    root_logger.setLevel(logging.DEBUG)
#    
#    
#    fh = logging.FileHandler("full_model.log")
#    fh_format = logging.Formatter('%(asctime)s - %(lineno)d - %(levelname)-8s: %(funcName)s - %(message)s')
#    fh.setFormatter(fh_format) #, datefmt="%Y-%m-%d %H:%M:%S"
#    root_logger.addHandler(fh)
#    
#    ch = logging.StreamHandler() #StreamHandler logs to console
#    ch.setLevel(logging.WARNING)
#    ch_format = logging.Formatter('%(asctime)s -  %(lineno)d - %(levelname)-8s: %(funcName)s - %(message)s')
#    ch.setFormatter(ch_format)
#    root_logger.addHandler(ch) 
    
    
    start_time = time.clock()
    current_date = datetime.now()
    print current_date
    print "RUNNING P-MODEL"
    date4saving =  str(current_date.day) + '_' + str(current_date.month)
    
    sYr = 1998
    sMo = 1
    sDa = 1
    eYr = 2001
    eMo = 12
    eDa = 31 
    
    start_date = date(sYr, sMo, sDa)
    end_date = date(eYr, eMo, eDa) 
    
    wn_nospin = np.tile(145.0, (360, 720))
    beta = 146.0
    phi_0 = 0.83
    absG = 1.0
    
    drivers = {}
    
    drivers['green_driver'] = 'fAPAR' # Other options: 'EVI'
    drivers['daily_splash_driver'] = 'watch_wfdei' #default using watch for daily tair and pn
    drivers['monthly_gpp_driver'] = 'cru' #Defualt using CRU for monthly sf (Splash), tair, tmin, tmax, vap
    drivers['annual_co2_driver'] = 'noaa_const'
    
    yr_drive = {}
    yr_drive['green_yr'] = 'const' # 
    yr_drive['splash_daily_yr'] = 'const' # 
    yr_drive['gpp_monthly_yr'] = 'const' # 
    yr_drive['co2_yr'] = 'const' # 

    
    od = {}        
    
    od['Vars2save'] = ('GPP','C13')
    od['Fnames'] =  (('gpp_' + drivers['green_driver'] + '_s' + str(start_date) + '_e' + str(end_date) + '_c' + date4saving),         
                      ('c13_' + drivers['green_driver'] + '_s' + str(start_date) + '_e' + str(end_date) + '_c' + date4saving))   
    
    od_test = {}

    test_gpp =  RUN_P_MODEL('test', 'global', start_date, end_date) 
    test_gpp.run_full_mod(start_date, end_date, drivers, wn_nospin, od_test, beta, phi_0, absG, yr_drive)
    gpp_out_98 = test_gpp.GPP_out
    precip_out_98 = test_gpp.PRECIP_out
    ppfd_out_98 = test_gpp.PAR_out
    tmp_out_98 = test_gpp.temp_out
    tmax_out_98 = test_gpp.tmax_out
    tmin_out_98 = test_gpp.tmin_out
    vap_out_98 = test_gpp.vap_out
    alpha_out_98 = test_gpp.alpha
    co2_out_98 = test_gpp.aCO2_out
    green_out_98 = test_gpp.green_out
#    aet_out = test_gpp.ET_out
    
    
   
  
    
    end_time = time.clock() - start_time
    print end_time, "seconds"        
