# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 10:39:07 2016

@author: rebecca
"""

import numpy as np
import calendar

import logging

class DAILY2MONTHLY:
    """
    Name: DAILY2MONTHLY
    FeatureS: gets the daily input variable for the year and makes monthly mean
    """
    
    def __init__(self, isLeap):
        
        self.logger = logging.getLogger(__name__)
        
        
        if isLeap:
            self.daysInYr = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        else:
            self.daysInYr = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        
    
    def d2m(self, varIn):
        
        yearly_vals = varIn
        
#        if calendar.isleap(yr):
#            del yearly_vals[59]
#            self.logger.info("Leap year. Deleting 29th Feb")
        d1 = varIn.shape[0]
       
        n_dims = varIn.ndim
        
        if n_dims == 1:
            sum_month = 0.0
            yearly_vals = np.float(yearly_vals)
            self.monthly_out = np.zeros(shape = (d1))
        elif n_dims == 2:
            d2 = varIn.shape[1]
            sum_month = np.zeros(shape = (d1, d2))
            self.monthly_out = np.zeros(shape = (d1, d2))
        elif n_dims == 3:
            d2 = varIn.shape[1]
            d3 = varIn.shape[2]
            sum_month = np.zeros(shape = (d1, d2, d3))
            self.monthly_out = np.zeros(shape = (d1, d2, d3))
            
        if d1 == 12:
            for im in range(d1):
                self.monthly_out[im,:,:] = yearly_vals[im] * self.daysInYr[im]
                
        else:
            zero_month = sum_month.copy() 
           
            mm = 0
            dd = 0.0
            for i in range(d1 + 1):
                
                sum_month += yearly_vals[i]
                dd += 1.0
                if dd == self.daysInYr[mm]:
                   self.monthly_out[mm] = sum_month/dd
                   mm += 1
                   dd = 0.0
                   sum_month = zero_month
                   
    #             if dd == self.daysInYr[mm]:
    #               self.monthly_out[mm] = sum_month/dd
    #               mm += 1
    #               dd = 0.0
    #               sum_month = zero_month
              
              
    #        self.monthly_out 
      
        
#####MAIN PROGRAM#######
        
if __name__ == '__main__':
    
    test_var = [0.0] * 365
    
    test_var[1] = 31.0
    test_var[59] = 2.0
    test_var[60] = 62.0
    test_var[61] = 93.0
    test_var[364] = 62.0
    
    yr = 2000
    
    test_d2m = DAILY2MONTHLY()
    test_d2m.d2m(test_var, yr)
    out_out = test_d2m.monthly_out
    
        