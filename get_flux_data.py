# -*- coding: utf-8 -*-
"""
Created on Fri Sep 30 16:46:18 2016

@author: rebecca
"""

import numpy as np
import os


class GET_FLUX_DATA:
    """
    Name:GET_FLUX_DATA
    Features: Finds the input data for a fluxnet site for a particular day and site
    """
    def __init__(self):
            """
            Name:     GET_FLUX_DATA.__init__
            Input:    None.
            Features: Initialise class variables
                      
            """
            self.precip = None
            self.tair = None
            self.sunf = None
            self.vapr = None
            self.fapar = None
          
          #        ### Class Function Definitions ####
   
   
    def get_temp (self, site_data_loc, yr, sitename):
        
        file_dir =  str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dtemp_' + sitename + '_' + str(yr) + '.txt')
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out    
            
    def get_sunf (self,site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dfsun_' + sitename + '_' + str(yr) + '.txt')
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out
#        self.sunf = t_out    
        
    def get_precip (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dprec_' + sitename + '_' + str(yr) + '.txt')
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        self.precip = t_out    
        return t_out
        
    def get_vapr (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dvapr_' + sitename + '_' + str(yr) + '.txt')  
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out   
        
    def get_vpd (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dvpd_' + sitename + '_' + str(yr) + '.txt')  
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out   
        
    def get_ppfd (self, site_data_loc, yr, sitename):
        
        
        file_dir = str(site_data_loc + 'climate/' + sitename + '/' + str(yr) + '/dppfd_' + sitename + '_' + str(yr) + '.txt')  
        try:
            os.path.isfile(file_dir)
        except:
            self.logger.info("no PPFD data avilable for site %s. Using SPLASH to calculate instead" %sitename)
            make_splash = True
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        make_splash = False
        return t_out, make_splash    
        
    def get_fapar (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'fapar/' + sitename + '/' + str(yr) + '/fapar_fapar3g_' + sitename + '_' + str(yr) + '.txt') 
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out    
        
    def get_evi (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'fapar/' + sitename + '/' + str(yr) + '/dfapar_evi_modissubset_' + sitename + '_' + str(yr) + '.txt') 
        t_out = []
        t_out = [line.strip() for line in open (file_dir)]
        return t_out    
        
    def get_co2 (self, site_data_loc, yr, sitename):
        
        file_dir = str(site_data_loc + 'co2/' + sitename + '/cCO2_rcp85_const850-1765.dat') 
        t_out = []
        t_out = [line.strip().split() for line in open (file_dir)]
        t_proc = t_out[yr - 850]
        co2_out = np.asarray(t_proc, dtype = np.float)[1] # Need to change this output form a string to a float
        return co2_out  
        
        
#            for i, line in enumerate(f):
#                t_out.append(f)