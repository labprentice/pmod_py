# -*- coding: utf-8 -*-
"""
Created on Fri Aug  5 15:21:01 2016

@author: rebecca
"""

from  main_model import RUN_P_MODEL
from constants import c_molmass
from run_site import RUN_SITE
import time
from datetime import date, datetime
import logging
from make_netcdf_file import make_netCDF
import numpy as np
import csv
import os
import errno

# Defining some logging functions (taken from http://stackoverflow.com/questions/11574257/how-do-i-write-log-messages-to-a-log-file-and-the-console-at-the-same-time)
# Alter sections 1, 2, 3. The rest should just work based on your selections here.

if __name__ == '__main__':
    
    # Initialise logging:
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    
    
    fh = logging.FileHandler("full_model.log")
    fh_format = logging.Formatter('%(asctime)s - %(lineno)d - %(levelname)-8s: %(funcName)s - %(message)s')
    fh.setFormatter(fh_format) #, datefmt="%Y-%m-%d %H:%M:%S"
    root_logger.addHandler(fh)
    
    ch = logging.StreamHandler() #StreamHandler logs to console
    ch.setLevel(logging.WARNING)
    ch_format = logging.Formatter('%(asctime)s -  %(lineno)d - %(levelname)-8s: %(funcName)s - %(message)s')
    ch.setFormatter(ch_format)
    root_logger.addHandler(ch) 
    
    CURRENT_DIR = os.path.abspath(os.curdir)
    PARENT_DIR = os.path.abspath(CURRENT_DIR + "/../") 
    GRANDPARENT_DIR = os.path.abspath(CURRENT_DIR + "/../../") 
    
    start_time = time.clock()
    current_date = datetime.now()
    print current_date
    print "RUNNING P-MODEL"
    date4saving =  str(current_date.day) + '_' + str(current_date.month)
    
    
    # 0. Pick if you want out to be global or at fluxnet sites, or both. 
    # Defualt is Global. Uncomment the relevant line.
    # If you choose fluxnet, you must already have run the code to get the climate for these.
    
    run_area = 'global'
 #   run_area = 'fluxnet'
#    run_area = 'both'    
    
    p_type = 'C3' # Alternative is 'C4'. This effects CO2 and phi_o
    # 1. Pick start and end date of run
    sYr = 2000
    sMo = 1
    sDa = 1
    eYr = 2000
    eMo = 12
    eDa = 31 
    
    start_date = date(sYr, sMo, sDa)
    end_date = date(eYr, eMo, eDa) 
    
    
    
    # 2. Choose the data you want to use to drive the model. This needs to be 
    #   saved in a folder of the same name and in the same folder as this code.
    
    simName = ('RT_' + run_area) #Change the simualtion name 
    drivers = {}
    
    drivers['green_driver'] = 'fAPAR' # Options: 'fAPAR', 'evi'
    drivers['daily_splash_driver'] = 'watch_wfdei' #default using watch for daily tair and pn
    drivers['monthly_gpp_driver'] = 'cru' #Defualt using CRU for monthly sf (Splash), tair, tmin, tmax, vap
    drivers['annual_co2_driver'] = 'noaa'
    
    #Now choose the years for the runs to take place over. If 'obs' is chosen, this will run over the start to end years
    # If 'const' is chosen, this will take the start year and repeat that year while looping through the days/months of the year
    yr_drive = {}
    yr_drive['green_yr'] = 'obs' # 
    yr_drive['splash_daily_yr'] = 'obs' # 
    yr_drive['gpp_monthly_yr'] = 'obs' # 
    yr_drive['co2_yr'] = 'obs' # 


    # 2.1 Change the values of phi_0 and beta
    beta = 146.0 # No unit  Ratio of carboxylation and transpiration costs at 25C. Default = 146.0
    if p_type == 'C3':
        p0 = 0.093 # mol/mol Long et al. 1993 # gC/mol Intrinisic quantum yield of photosynthesis. Defualt = 0.83, site scale =  0.093 * c_molmass
    else:
         p0 = 0.055  # Long et al 1993. For C4 plants
         
    phi_0 = p0 * c_molmass
    
    if drivers['green_driver'] == 'fAPAR':
        absG = 0.8
    else:
        absG = 1.0
        
    alpha = False #Currently not sure that it correct to have alpha^3/4, so don't include it for now
    
    # 3. Choose what output you want. [1] = ouptut, [0] = not output
    
    saving_2_netcdf = True
    saving_2_txt = False
    
    outr = {}
    
    outr['GPP'] = True
    outr['LUE'] = True
    outr['C13'] = False
    outr['PAR'] = False # Monthly ppfd
    outr['PPFD'] = False # daily ppfd
    outr['TEMP'] = False
    outr['NET_RAD'] = False #output from SPLASH: net radiation
    outr['PRECIP'] = False  #from SPLASH(data input_: precipitation
    outr['ET'] = False  #From SPLASH: evapotranspiration
    outr['AET'] = False
    outr['PET'] = False
    outr['VPD'] = False
    outr['SoilM'] = False
    
    
        
    
    # 4. These are the units of the output files: ####
    units = {}
    
    units['GPP'] = 'gC m-2 month-1' 
    units['LUE'] = 'none'
    units['C13'] = 'per mil'
    units['PAR'] = 'mol/m^2/month'
    units['PPFD'] = 'mol/m^2/day'
    units['TEMP'] = 'celcius'
    units['NET_RAD'] = 'J/m2'
    units['PRECIP'] = 'mm'
    units['ET'] = 'mm'
    units['AET'] = 'mm'
    units['PET'] = 'mm'
    units['VPD'] = 'Pa'
    units['SoilM'] = 'mm'
    
    
   
    # 5. Run the P model using spun up soil moisture (wn) for the location specified (global, site-level or both)
    
    if run_area == 'global':
            root_logger.info("Global model selected")
            
            P_MOD = RUN_P_MODEL(simName, run_area, p_type, start_date, end_date)
         # a. spin up the soil moisture everywhere
               
            if alpha:
                spun_wn = P_MOD.run_spin_up_gridded(start_date, drivers)
                          
            
                root_logger.info("Spin up done")
            else:
                spun_wn = False
             # b. Run the full model
            root_logger.info("Running global P-model")
            P_MOD.run_full_mod(start_date, end_date, drivers, outr, beta, phi_0, absG, yr_drive, spun_wn)
            
    elif run_area == 'fluxnet':
             root_logger.info("Site-scale model selected")
             
             P_MOD = RUN_P_MODEL('CH-Oe1_RT', run_area)
             P_MOD.run_site_mod(beta,
                             phi_0,
                             absG,
                             outr,
                             site_list_loc = 'siteparamfils/site_paramfils/',
                             site_data_loc = 'input/input_fluxnet_sofun/sitedata/',
                             param_data_loc = 'siteparamfils/run/')
                             
            
    elif run_area == 'both':
         print("Haven't coded this yet")
    
    ## Everything is always output to these dictionaries but they are only saved if you have specified above
    
    outd = {}
    
    try:
       outd['GPP'] = P_MOD.GPP_out
       root_logger.info('GPP saved')
    except:
       root_logger.info("GPP not saved")
    try:
       outd['LUE'] = P_MOD.LUE_out
       root_logger.info('LUE saved')
    except:
       root_logger.info("LUE not saved")
    try:
      outd['C13'] = P_MOD.C13_out
      root_logger.info('C13 saved')
    except:
       root_logger.info("C13 not saved")
    try:
      outd['PAR'] = P_MOD.PAR_out 
      root_logger.info('PAR saved')
    except:
       root_logger.info("PAR not saved")
    try:
       outd['PPFD'] = P_MOD.PPFD  #This is claucated PPFD in Splash model
       root_logger.info('PPFD saved')
    except:
       root_logger.info("PPFD not saved")
    try:
       outd['LUE'] = P_MOD.site_run.site_lue 
    except:
       root_logger.info("LUE not saved")
    try:
       outd['TEMP'] = P_MOD.site_run.site_temp
    except:
       root_logger.info("Temp not saved")
    try:
       outd['NET_RAD'] = P_MOD.NETRAD_out
       root_logger.info('Net rad saved')
    except:
       root_logger.info("Net rad not saved")
    try:
       outd['PRECIP'] = P_MOD.PRECIP_out
       root_logger.info('Precip saved')
    except:
        root_logger.info("Precip not saved")
    try:
       outd['Green'] = P_MOD.Green_out
       root_logger.info('fAPAR saved')
    except:
       root_logger.info("fAPAR not saved")
   ####
    try:
       outd['AET'] = P_MOD.aet_out
       root_logger.info('AET saved')
    except:
       root_logger.info("AET not saved")
    try:
       outd['PET'] = P_MOD.pet_out
       root_logger.info('PET saved')
    except:
       root_logger.info("PET not saved")
    try:
       outd['VPD'] = P_MOD.vpd
       root_logger.info('VPD saved')
    except:
       root_logger.info("VPD not saved")
    try:
       outd['SoilM'] = P_MOD.daily_sm
       root_logger.info('Soil Mositure saved')
    except:
       root_logger.info("Soil Moisture not saved")
    
    
    
    
    # 7. Save the output
    
    
    saving_keys = outr.keys()
    saving_v = outr.values()
    
    saveVar = []
    for mm in range(len(saving_v)):
        if saving_v[mm] == 1:
            saveVar.append(saving_keys[mm])
                
                
                
    if saving_2_txt:
        readme_info = []
        readme_info.append('### This readme contains information about the variables saved, the units and the date they were created ###')
        readme_info.append('### Date created: ' + str(current_date) + ' ###')
        readme_info.append('Greening data used = ' + drivers['green_driver'])
        readme_info.append('Photosynthetic pathway = ' + p_type)
        
        for mm in range(len(saveVar)):
            vv = saveVar[mm]
            site_data = outd[vv]
            unit_o = units[vv]
            sitename = site_data.keys()[0]
            saveFile = (simName + '_' + sitename + '_' + str(vv) +'.csv')
            
            #Create new folder for each site if it doens't exist
            save_path = os.path.join(PARENT_DIR + '/output/' + sitename)
            if not os.path.exists(save_path):
                try:
                    os.makedirs(save_path)
                except OSError as exc: # Guard against race condition
                    if exc.errno != errno.EEXIST:
                            raise
            readme_info.append('Variable: ' + vv + ': ' + unit_o)       
            
            all_data = site_data.values()[0]
            full_path = os.path.join(save_path + '/' + saveFile)
            
            with open(full_path, 'wb') as write_file:
                file_writer = csv.writer(write_file)
                for i in range(len(all_data[0])):
                    file_writer.writerow([x[i] for x in all_data])
                       
            
        #Save the README
        readme_path = os.path.join(save_path + '/' + 'README.txt')
        with open(readme_path, 'wb') as file_in:
            for row in readme_info:
             file_in.write("%s\n" %row)
    
    if saving_2_netcdf:
   
       
        write_data = make_netCDF()
        
        for mm in range(len(saveVar)):
            vv = saveVar[mm]
            varName = str(vv)
            saveFile = (simName + varName + '_' + p_type + '_s' + str(start_date) + '_e' + str(end_date) + '_r' + date4saving)
            unit_o = units[vv]
            data2write = outd[vv]
            write_data.make_the_file(saveFile, data2write, varName, P_MOD.data.latitude, P_MOD.data.longitude, start_date, unit_o, p_type, p0, beta, drivers['green_driver'])    
       
       
        
    root_logger.info("FINISHED!")
    end_time = time.clock() - start_time
    print end_time, "seconds"                      
        
