# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 12:47:53 2016

@author: rebecca
"""
from datetime import date, timedelta
from get_flux_data import GET_FLUX_DATA
from splash import SPLASH
import numpy as np

import logging

class RUN_SPIN_SITE:
    """
    Name: RUN_SPIN_SITE
    Features: runs the splash model to spin up soil moisture for a given site
    """
 
    def __init__(self):
        """
        Name: RUN_SPIN_SITE.__init__
        Input: none
        """
        self.logger = logging.getLogger(__name__)
        self.logger.info("Running P-model site level spin-up")

    def run_spin_up(self, sitename, spin_yrs,  site_lat, site_lon, site_elv, start_yr,  site_data_path):
        
        self.wn = 145.0    
        self.wn_all = np.zeros(365 * spin_yrs)
        my_day = 0
        
        start_date_spin = date(start_yr, 1, 1)
        end_date_spin = date(start_yr, 12, 31)

        self.logger.debug("Running spin-up from %s to %s for %s yrs" % (str(start_date_spin), str(end_date_spin), str(spin_yrs)))
  
        #Get the data for the spin up - currently jsut using one year so read it all in here
        self.flux_data = GET_FLUX_DATA()
        
        site_sf = self.flux_data.get_sunf(site_data_path, start_yr, sitename)
        site_tair = self.flux_data.get_temp(site_data_path, start_yr, sitename)
        site_precip = self.flux_data.get_precip(site_data_path, start_yr, sitename)
        
        
        for sp_yr in range(spin_yrs): #Run the spin up for specified number of years  
            for my_date in self.daterange_daily(start_date_spin, end_date_spin):
                        
                doy = my_date.timetuple().tm_yday
                year_spin = my_date.year
                
                # This is wrong - need to ignore day 29 in Feb rahter than alst day of year.
                if doy == 366:
                    no_leap_year = False
                else:
                    no_leap_year = True
                        
                if no_leap_year:                   
                    today_sf = np.asarray(site_sf, dtype=np.float)[doy-1]
                    today_tair = np.asarray(site_tair, dtype=np.float)[doy-1] 
                    today_precip = np.asarray(site_precip, dtype=np.float)[doy-1]
                    today_wn = np.asarray(self.wn, dtype=np.float)
                    
                    self.logger.debug("Running for DOY %s" %str(doy))   
                    
                    self.make_splash = SPLASH(site_lat, site_elv, site_lon)
                    self.make_splash.run_one_day(doy, year_spin, today_wn , today_sf, today_tair , today_precip, leap_const = False )
                    self.wn = self.make_splash.wn
                    self.wn_all[my_day] = self.wn
                    my_day += 1
                    self.logger.debug("calculated global soil moisture for %s" %str(my_date))  
                    
    def daterange_daily(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)  
            
                
############ MAIN PROGRAM ##############

if __name__ == '__main__':
    
  

    wn = 145.0
    site_lat = 47.11667
    site_lon = 11.3175
    site_elv = 970
    start_yr = 2000
    end_yr = 2001
    spin_yrs = 4
    site_data_path = ('/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/input/input_fluxnet_sofun/sitedata/')
    
    sitename = 'AT-Neu'
    
    start_date_run = date(start_yr, 1,1)
    end_date_run = date(end_yr, 12, 31)
    
    base = start_date_run
    diff = end_date_run - start_date_run 
    numdays = diff.days
    date_list_daily = [base + timedelta(days = x) for x in range(0, numdays)]
    
    do_spin = RUN_SPIN_SITE()
    do_spin.run_spin_up(sitename, spin_yrs,  site_lat, site_lon, site_elv, start_yr,  site_data_path)
    wn_out = do_spin.wn_all
    