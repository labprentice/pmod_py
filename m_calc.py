# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 16:44:57 2016

@author: GreencyclesII

testing the calculation of m (can do this without having SPLASH working)
"""
# TO DO:
# 1. Import Ca (CO2), Tk (air temp), Tmax, Tmin, monthly vapour pressure, z (elevation)
# 2. Initialise grid??
# 3. Calculate photorespiratory compensation point (gamma*)
# 4. Calcuate Viscosity of water (eta *)
# 5. Calcualte VPD (D)
# 6. Calcuate K1 --> requires Kc, Po, Ko
# 7. Calculate Kc
# 8. Calulate Po
# 9. Calculate Ko
# 10. When all above calculated--> calculate m
# 11. TO OUTPUT: m only 

# Input: Ca (CO2), Tk (air temp), Tmax, Tmin, monthly vapour pressure, z (elevation)
# Output: m



import numpy as np

import os
import sys

#CURRENT_DIR = os.path.abspath(os.curdir)
#PARENT_DIR = os.path.abspath(CURRENT_DIR + "/../") 
        
myhome = os.path.expanduser("~")

evap_path = os.path.join(myhome, 'GPP_mod/SPLASH/working/py_version') # the SPLASH code location
sys.path.append(evap_path)   


from evap import EVAP

from constants import gamma_25, t_25, Ha, Ha_kc, Ha_ko, R, eta_const, kco, kPo, ko25, kc25, es0

class M_CALC:
    """
    Name: M_CALC
    Features: Calculates M for GPP
    """
    
    def __init__(self):
            """
            Name:     M_CALC.__init__
            Input:    - float, latitude, degrees (lat)
                      - float, latitude, degrees (lon)  
                      
            """
            self.m = None
            self.vpd_Pa = None
        
        #### Class Function dEfinitions ####
        
    def run_grid(self, temp, aCO2, elv, beta, tMax, tMin, vap, vpd):
            """
            Name: M_CALC.run
            Input: - float, ambient CO2 (aCO2)
            Outputs: - float, M (for GPP claucation)
            Depends: - GAMMA_STAR
                     - ETA_STAR
                     - VPD_CALC
                     - K1_CALC
            """
            evap = EVAP(25.0, elv)
            self.patm = evap.elv2pres(elv)
            
            
            pp_CO2 = aCO2 * 1E-6 * self.patm #* 101.325 * 1000 # CO2 in Pa--> partial pressure for average atmospheroc pressure 
                                                  # CO2 ppm *  1/10^-6 * 
                       
            if type(temp) == np.float64:
                kelv2cel = 273.15
            else:
                kelv2cel = np.tile(273.15, (pp_CO2.shape[0], pp_CO2.shape[1]))
                
                
            # Calcuate VPD is not given as data input
            if vpd is False:
                 this_vpd = self.calc_vpd(tMax, tMin, vap)
            else:
                 this_vpd = vpd
                 
                                 
            k_temp = temp + kelv2cel  #temp in kelvin
            #k_temp25 = 25.0 + kelv2cel
           
            
            m_calc = (pp_CO2 - self.gamma_star(k_temp)) / \
                     (pp_CO2 + (2 * self.gamma_star(k_temp)) + \
                     (3 * self.gamma_star(k_temp)) * \
                     np.sqrt(1.6 * self.eta_star(temp, self.patm, 25.0, kPo, elv) * \
                     this_vpd * \
                     (beta ** -1.0) * \
                     ((self.k1(k_temp, elv, self.patm) + self.gamma_star(k_temp)) ** -1.0))) 
                 
          
            self.vpd_Pa = this_vpd   
            self.m = m_calc
            return m_calc
            
    def gamma_star(self,k_temp):
            """
            Name: GAMMA_STAR.run
            Input: -Float, Air temp (Tk) Given in degC, need kelvin
            Output: - Float, Gamma*
            """
            ttg=((k_temp - t_25) * Ha) / (R * k_temp * t_25)
            gs=gamma_25 * np.exp(ttg)
            self.GSTAR = gs
            
            return gs
        
    def eta_star(self,temp, patm, temp25, kPo, elv):
            """
            Name: ETA_STAR.run
            Input: -Float, Air temp (Tk)
            Output: - Float, ETA*
            """
            # Calculated as eta/eta25 
            # viscosity correction factor = viscosity( temp, press )/viscosity( 25 degC, 1013.25 Pa) 
            
            #es = np.exp(eta_const * (k_temp - t_25))
            
            evap = EVAP(25.0, elv)
            
            self.ns = evap.viscosity_h2o(temp, patm)
            self.ns25 = evap.viscosity_h2o(temp25, kPo)
            
            es = self.ns/self.ns25
            self.ESTAR = es
#            print es
            return es
            
    def calc_vpd(self,tMax, tMin, vap):
            """
            Name: VPD_CALC.run
            Input: -Float, monlty average daily maximum temp (Tmax) deg C
                   -Float, monlty average daily minimum temp (Tmin) deg C
                   -Float, monlty average vapour pressure temp (vap) hPa (1 Pa = 0.01 hPa)
                   Output: - Float, D (jn equation)
            """
            
            tMaxMin=(8.635 * (tMax + tMin))/(0.5 * (tMax + tMin) + 237.3)
        
            vpd_out=(es0 * np.exp(tMaxMin) - (0.10 * vap)) * 1000 # kPa to Pa (units for m are all Pa)
                   
            return vpd_out
            
    def k1(self, k_temp, elv, patm):
             """
             Name:     K0_CALC.__init__
             Input:    - float, latitude, degrees (lat)
                      - float, latitude, degrees (lon)  
                      -Float, monlty average temperature (Tk)
                      -Float, elevation at grid square (elv)
                      Output: - Float, K1
            Depends: - Kc
                     - P0
                     - K0
             """
             k1_out = self.kc(k_temp) * (1 + ( self.p0(elv, self.patm) / self.k0(k_temp)))
             self.K_1 = k1_out
             return k1_out
            
    def kc(self,k_temp):
    
            """
            Name: k1_calc.kc
            Input: -Float, monlty average temperature (Tk)
                   - Const = 79.430 kJ mol-1 = energy of activation for carboxylation
            Output: - Float, Kc
            """
            
            #dhatKc = 79430 #J/mol
            #kc25 = 39.97 # Pa at 25 deg C and 98.716KPa
            
            tempFrac=((k_temp - t_25) * Ha_kc) / (R * k_temp * t_25)
            
            Kc_out = kc25 * np.exp(tempFrac)
        
            return Kc_out
        
    def p0(self,elv, patm):
            """
            Name: k1_calc.po
            Input: -Float, elevation at grid point
            Output: - Float, P0. O2 partial pressure
            """
            
            #P0_out=21000  * np.exp( - 0.114 * (elv * 1E-3))
            P0_out = kco * (1e-6) * patm
            
            return P0_out
        
    def k0(self,k_temp):
            """
            Name: K0_CALC.run
            Input: -Float, monlty average temperature (Tk)
                  - Const = 36.380 kJ mol-1 = energy of activation for oxygenation
            Output: - Float, K1
            """
            
            #dhatK0 = 36380 # J/mol
            #k025 = 27480 # Pa, at 25 deg C and 98.716KPa
            
            tempFracK0=((k_temp - t_25) * Ha_ko) / (R * k_temp * t_25)
            
            K0_out=ko25 * np.exp(tempFracK0)
            
            return K0_out
              
    def print_vals(self):
            
            print " m: %0.10f" % (self.m)
        
#    def run(self, aCO2):
#        """
#        Name: M_CALC.run
#        Input: - float, ambient CO2 (aCO2)
#        Outputs: - float, M (for GPP claucation)
#        Depends: - GAMMA_STAR
#                 - ETA_STAR
#                 - VPD_CALC
#                 - K1_CALC
#        """
##        # 1. Calculate GAMMA_*
##        my_gamma = GAMMA_STAR(self.lat, self.lon)
##        my_gamma.gs=my_gamma.run(self.temp)
##        self.gamma_star = my_gamma.gs
#        

#        






