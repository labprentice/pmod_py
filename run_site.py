# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 17:18:39 2016

@author: rebecca
"""

import csv
from datetime import timedelta, date, datetime
import sys
import numpy as np
import os
import logging

CURRENT_DIR = os.path.abspath(os.curdir)
PARENT_DIR = os.path.abspath(CURRENT_DIR + "/../") 
GRANDPARENT_DIR = os.path.abspath(CURRENT_DIR + "/../../") 
        
splash_path = os.path.join(PARENT_DIR, 'SPLASH/working/py_version') # the SPLASH code location
sys.path.append(splash_path)         

from run_spin_site import RUN_SPIN_SITE
from run_pmod_site import RUN_PMOD_SITE

class RUN_SITE:
    
    def __init__(self):
        """
        Name:  RUN_SITE.__init__
        Input: Site info
        Features: Run the P_model for all sites listed in a file in a loop2
        """
        
        self.logger = logging.getLogger(__name__)
        self.logger.info("Initialising P-model at site level")
        
        # Initalising output variables
        self.site_gpp = {}
        self.site_temp = {}
        self.site_ppfd = {}
        self.site_lue = {}
        self.site_gpp_date = {}
       
        
        
    def run_site(self, beta, phi_0, absG, outr, site_list_path, site_data_path, param_data_path):
       # Get the input site data
       
#        my_site = ()
#       with open(site_list_path, 'rb') as csvfile:
#           site_list_in = csv.reader(csvfile, delimiter = ',')
#           
##          
#            # This gets all the site info and removes the header
#           firstline = True
#           for row in site_list_in:
#               if firstline:
#                   firstline = False
#                   continue
#               sitename = row[0]
#               site_lon_str = row[1]
#               site_lat_str = row[2]
#               site_elv_str = row[3]
          site_list_info = os.listdir(site_list_path)
          
          for si in range(len(site_list_info)):
               
               site_file = site_list_info[si]
               
               if site_file.startswith("."):
                   sitename = 'Not a site'
                   self.logger.info("Skipping file called %s" %site_file)
               else:
                   site_split = site_file.split(".")
                   sitename = site_split[0]
          
                   self.logger.info("Got site data for %s" %sitename)
                  
                   
                 
                 # Now find the lat, lon, elv data in the parameter files
                 
                   my_site_info = str(site_list_path + site_file)
                   
                   print my_site_info
                   site_lat, site_lon, site_elv = self.get_site_basic_info(sitename, my_site_info)
                   
                 
                   
                    # Run the P model in here for each site
                   # SPIN UP
                   #Find the data
                   
                   spin_yrs, recycle_yrs, start_yr, end_yr = self.get_site_spin_info(sitename, param_data_path)
                   
                   
                   start_date_run = date(start_yr, 1, 1)
                   end_date_run = date(end_yr, 12, 31)
                   
                   
#                   base = start_date_run
#                   diff = end_date_run - start_date_run 
#                   numdays = diff.days
#                   date_list_daily = [base + timedelta(days = x) for x in range(0, numdays)]
                   
                   self.run_spin_up_site = RUN_SPIN_SITE()
                   self.run_spin_up_site.run_spin_up(sitename, spin_yrs, site_lat, site_lon, site_elv, start_yr, site_data_path)
                   
                   spun_wn = self.run_spin_up_site.wn
                   
                   
                   self.run_pmod = RUN_PMOD_SITE(start_date_run, end_date_run)
                   self.run_pmod.run_pmod(sitename,  site_lat, site_lon, site_elv, start_date_run, end_date_run, site_data_path, beta, phi_0, absG, spun_wn)
                   
                   
                   self.site_gpp[sitename] = (self.run_pmod.my_date, self.run_pmod.GPP_out)
                   
                   self.site_ppfd[sitename] = (self.run_pmod.my_date, self.run_pmod.ppfd_out)
                   self.site_temp[sitename] = (self.run_pmod.my_date, self.run_pmod.temp_out)
                   self.site_lue[sitename] = (self.run_pmod.my_date, self.run_pmod.lue_out)
                    
               
    
    def get_site_basic_info(self, sitename, my_site_info):
        
        
        with open (my_site_info, 'rb') as f:
            for line in f:
                info = line.split()
  
                try:
                       n = info.index('longitude')
                       longitude = float(info[n+1])
                except ValueError:
                       self.logger.info ("not in this line")
                       
                try:
                       n = info.index('latitude')
                       latitude = float(info[n+1])
                except ValueError:
                       self.logger.info ("not in this line")
                       
                try:
                       n = info.index('altitude')
                       elv = float(info[n+1])
                except ValueError:
                       self.logger.info ("not in this line")
                       
        return longitude, latitude, elv
                
           
    def get_site_spin_info(self, sitename, param_data_path):
        
        site_param_file = str(param_data_path + sitename + '.sofun.parameter')
        print site_param_file
        
        try:
            os.path.isfile(site_param_file)
            
        except:
                self.logger.debug("Paramter file does not exist for %s" %sitename)
                print "Parameter file does not exist"
    #        line_list = []
        with open (site_param_file, 'rb') as f:
            for line in f:
                info = line.split()
                
                
                try:
                       n = info.index('spinupyears')
                       spin_yrs = int(info[n+1])
                except ValueError:
                       self.logger.info ("not in this line")
                
                try: 
                    n = info.index('recycle')
                    recycle_yrs = int(info[n+1])
                except ValueError:
                    self.logger.info ("not in this line")
                    
                try:
                    n = info.index('daily_out_startyr')
                    start_yr = int(info[n+1])
                except ValueError:
                    self.logger.info ("not in this line")
                
                try:
                    n = info.index('daily_out_endyr')
                    end_yr = int(info[n+1])
                except ValueError:
                    self.logger.info ("not in this line")
              
        return spin_yrs, recycle_yrs, start_yr, end_yr
    
                
               
    
    def daterange_daily(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)):
            yield start_date + timedelta(n)  
            
    def daterange_year(self, start_date, end_date):
            for n in range(int ((end_date - start_date).years)):
                yield start_date + timedelta(n)  
                
                
############ MAIN PROGRAM ##########
                
if __name__ == '__main__':
    
    beta = 146.0
    phi_0 = 0.83
    absG = 1.0
    
    site_list_path ='/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/siteparamfils/site_paramfils/'             
     
    site_data_path = ('/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/input/input_fluxnet_sofun/sitedata/')
    param_data_path = ('/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/siteparamfils/run/')
    
    outr = {}
    outr['GPP'] = True
    
    test_site_run = RUN_SITE()
    test_site_run.run_site(beta, phi_0, absG, outr, site_list_path, site_data_path, param_data_path)
    test_output = test_site_run.site_gpp
              