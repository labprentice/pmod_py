# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 13:54:03 2016

@author: rebecca
"""

######## Import modules#########

import numpy as np
from constants import a_hat, b_hat


class C13_CALC:
    """
    Name: C13_CALC
    Features: Calculates GPP for given data
    """
    def __init__(self):
            """
            Name:     C13_CALC.__init__
            Input:    None.
            Features: Initialise class variables
                      
            """
            self.c13 = None
          
          #        ### Class Function Definitions ####
    def find_c13(self, aCO2, gamma_star, vpd, k1, eta_star, beta):
         """
            Name: C13_CALC.find_c13
            Input: - float, ambient CO2 (aCO2)
                   
            Outputs: - float, c13 discrimination
          """
         pp_CO2 = aCO2 * 1E-6 * 101.325 * 1000 # CO2 in Pa--> partial pressure for average atmospheroc pressure 
                                                  # CO2 ppm *  1/10^-6 * 
         self.ci_ca = self.ci(pp_CO2, gamma_star, vpd, k1, eta_star, beta)/pp_CO2
         self.c13 = (self.ci_ca * (b_hat - a_hat)) + a_hat

#         self.ci_ca_old = self.chi_old(pp_CO2, gamma_star, vpd, k1, eta_star, beta)/pp_CO2
         
    def ci(self, pp_CO2, gamma_star, vpd, k1, eta_star, beta):
         """
            Name: C13_CALC.ci 
            Input: - float, ambient CO2 (aCO2)
                   - float, vpd
            Outputs: - float, internal CO2 conetration
            Depends: - ZETA
                     - GAMMA_STAR
                     - VPD
          """
        
         top_frac = (self.zeta(k1, gamma_star, eta_star, beta) * pp_CO2) + (gamma_star * np.sqrt(vpd))
         bottom_frac = self.zeta(k1, gamma_star, eta_star, beta) + np.sqrt(vpd)
        
         ci_o = top_frac/bottom_frac
         self.ci_out = ci_o
         return ci_o
         
    def chi_old(self, pp_CO2, gamma_star, vpd, k1, eta_star, beta):
         """
            Name: C13_CALC.ci 
            Input: - float, ambient CO2 (aCO2)
                   - float, vpd
            Outputs: - float, internal CO2 conetration
            Depends: - ZETA
                     - GAMMA_STAR
                     - VPD
          """
        
         top_frac = self.zeta_old(k1, gamma_star, eta_star, beta) 
         bottom_frac = self.zeta_old(k1, gamma_star, eta_star, beta) + np.sqrt(vpd)
        
         chi_o_old = top_frac/bottom_frac
         self.chi_out_old = chi_o_old
         return chi_o_old
        
    def zeta(self, k1, gamma_star, eta_star, beta):
         """
            Name: C13_CALC.zeta_star 
            Input: 
            Outputs: - float, zeta_star sensitivity of ci/ca to VPD
            Depends: - BETA
                     - K1
                     - GAMMA_STAR
                     - ETA_STAR
          """
          
         frac_zeta = (beta * (k1 + gamma_star)) / (1.6 * eta_star)
         zeta_o = np.sqrt(frac_zeta)
          
         return zeta_o
         
    def zeta_old(self, k1, gamma_star, eta_star, beta):
         """
            Name: C13_CALC.zeta_star 
            Input: 
            Outputs: - float, zeta_star sensitivity of ci/ca to VPD
            Depends: - BETA
                     - K1
                     - GAMMA_STAR
                     - ETA_STAR
          """
          
         frac_zeta = (beta * k1) / (1.6 * eta_star)
         zeta_o_old = np.sqrt(frac_zeta)
          
         return zeta_o_old