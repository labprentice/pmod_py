# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 12:53:07 2016

@author: rebecca
"""

from datetime import date, timedelta, datetime
import numpy as np
import os
import sys
import logging

CURRENT_DIR = os.path.abspath(os.curdir)
PARENT_DIR = os.path.abspath(CURRENT_DIR + "/../") 
GRANDPARENT_DIR = os.path.abspath(CURRENT_DIR + "/../../") 
        
splash_path = os.path.join(PARENT_DIR, 'SPLASH/working/py_version') # the SPLASH code location
sys.path.append(splash_path)    

from get_flux_data import GET_FLUX_DATA
from splash import SPLASH
from gpp_calc import GPP_CALC
from daily2monthly import DAILY2MONTHLY
from dateutil.relativedelta import relativedelta
import calendar


class RUN_PMOD_SITE:
    
    def __init__(self, start_date_run, end_date_run):
        """
        Name: RUN_PMOD_SITE.__init__
        Input: none
        """
        self.logger = logging.getLogger(__name__)
        self.logger.info("Running P-model at site level" )
        
        
        self.run_range_mo = relativedelta(end_date_run  + timedelta(1), start_date_run)
        noMonths =  (self.run_range_mo.years * 12) + self.run_range_mo.months
        self.run_range = end_date_run - start_date_run
        noDays = self.run_range.days
        
        # minus off any leap year days becuas these are not included in the input data
        # Calculate the number of leap year days
        leap_days = calendar.leapdays(start_date_run.year, end_date_run.year)
        noTime_d = noDays - leap_days
        noTime_m = noMonths
      
        self.GPP_out = np.zeros(shape = (noTime_d + 1))
        self.pet_out = np.zeros(shape = (noTime_d + 1))
        self.ppfd_out = np.zeros(shape = (noTime_d + 1))
        self.temp_out = np.zeros(shape = (noTime_d + 1))
        self.vpd_out = np.zeros(shape = (noTime_d + 1))
        self.lue_out = np.zeros(shape = (noTime_d + 1))
        self.fapar_out = np.zeros(shape = (noTime_d + 1))
        self.co2_out = np.zeros(shape = (noTime_d + 1))
        self.M_out = np.zeros(shape = (noTime_d + 1))
        self.my_date = np.zeros(shape = (noTime_d + 1))
        
        
    def run_pmod(self, sitename,  site_lat, site_lon, site_elv, start_date_run, end_date_run, site_data_path, beta, phi_0, absG, wn_in):
            
            tNo =  0
            gpp_out = 'daily'
            self.logger.info("Running full p_model from %s to %s for %s" % (str(start_date_run), str(end_date_run), sitename)) 
           
            self.wn = wn_in
            
           
            for current_yr in range(start_date_run.year, end_date_run.year + 1): #Run over these years
                
#                # I would use lyear flag if I wanted to include leapyears in the site comaprison
#                if calendar.isleap(current_yr):
#                   lyear = True
#                else:
#                    lyear = False
#                    
                
                
                self.flux_data = GET_FLUX_DATA()
             
                site_sf = self.flux_data.get_sunf(site_data_path, current_yr, sitename)
#                site_sf = make_gpp.flux_data.sunf
                site_tair = self.flux_data.get_temp(site_data_path, current_yr, sitename)
                site_precip = self.flux_data.get_precip(site_data_path, current_yr, sitename)
                site_CO2 = self.flux_data.get_co2(site_data_path, current_yr, sitename)
#                site_vap = self.flux_data.get_vapr(site_data_path, current_yr, sitename)
                site_vpd = self.flux_data.get_vpd(site_data_path, current_yr, sitename)
                site_fapar = self.flux_data.get_evi(site_data_path, current_yr, sitename)
                site_ppfd, make_splash = self.flux_data.get_ppfd(site_data_path, current_yr, sitename)
                
                self.CO2 = site_CO2
                 
                self.sf_out = site_sf
                
                d_2_m_tair = DAILY2MONTHLY(isLeap = False)
                d_2_m_tair.d2m(site_tair, current_yr)
                monthly_tair = d_2_m_tair.monthly_out
                
                d_2_m_vpd = DAILY2MONTHLY(isLeap = False)
                d_2_m_vpd.d2m(site_vpd, current_yr)
                monthly_vpd = d_2_m_vpd.monthly_out
                
#                print("Monthly tair")
#                print monthly_tair
#                print ("Monthly vpd")
#                print monthly_vpd
                
#                month_aet = np.empty(31)
#                month_eet = np.empty(31)
#                month_ppfd = np.empty(31)
#                month_temp = np.empty(31)
#                month_vpd = np.empty(31)
#                    month_vap = np.empty(31)
                
                this_yr_start = date(current_yr, 1, 1)
                this_yr_end = date(current_yr + 1, 1, 1)
                
#                if calendar.isleap(this_yr_start.year):
#                        leap_day_ = 1.0
#                else:
#                        leap_day_ = 0.0
#                        
#               
                
                for my_date in self.daterange_daily(this_yr_start, this_yr_end): # - timedelta(leap_day_)):
                    
                    
                    doy = my_date.timetuple().tm_yday
                    tomorrow = my_date + timedelta(1)
                    month = my_date.month
                    
                    date_frac = my_date.year + doy/365.0
                    n = 1
                   
                    self.logger.debug("Running P_Mod for %s" %str(my_date))       
                    self.logger.debug("DOY is %s" %(doy))
                        
                    if calendar.isleap(my_date.year) and doy > 59:
                           n = 2
                           
                    
                            
                        
#                    print ("doy is %f, month is %f, sf is %s") %(tNo, month, site_sf[tNo])
                        
                    today_sf = np.asarray(site_sf, dtype=np.float)[doy - n]
                    today_precip = np.asarray(site_precip, dtype=np.float)[doy - n]
                    today_wn = np.asarray(self.wn, dtype=np.float)
                    this_fapar = np.asarray(site_fapar, dtype=np.float)[doy - n]   
                    
                    today_tair = np.asarray(monthly_tair, dtype=np.float)[month-1]                     
                    today_vpd = np.asarray(monthly_vpd, dtype=np.float)[month-1]
                    
                    if make_splash:
                          
                        self.make_splash = SPLASH(site_lat, site_elv, site_lon)
                        self.make_splash.run_one_day(doy, current_yr, today_wn , today_sf, today_tair , today_precip, leap_const = False )
                        self.wn = self.make_splash.wn
                        self.logger.debug("calculated global soil moisture for %s" %str(my_date))  
            
#                        if self.t_int == 'monthly':                        
#                            month_aet[my_date.day - 1] = self.make_splash.aet
#                            month_eet[my_date.day - 1] = self.make_splash.eet
#                            month_ppfd[my_date.day - 1] = self.make_splash.ppfd_d
#                            month_temp[my_date.day - 1] = today_tair
#                            month_vpd[my_date.day - 1] = today_vpd
#                            today_vap = np.asarray(site_vap, dtype=np.float)[doy-1]
                            
#                            month_vap[my_date.day - 1] = today_vap
#                        else:
                        this_ppfd = self.make_splash.ppfd_d
                           
                    else:
                         this_ppfd = np.asarray(site_ppfd, dtype=np.float)[doy - n]
                    
                    if month <> tomorrow.month or my_date == end_date_run or gpp_out == 'daily':
                        get_gpp = True
                        
                    else:
                        get_gpp = False     
              
          
                    if get_gpp:
                       
                        
#                        if self.t_int == 'monthly':
#                            self.sum_month_aet = np.nansum(month_aet,0)
#                            self.sum_month_eet = np.nansum(month_eet, 0)
#                            self.site_alpha = self.sum_month_aet/self.sum_month_eet
#                            if np.isinf(self.site_alpha):
#                                self.site_alpha = np.nan
#                            
#                            this_ppfd = np.nansum(month_ppfd, 0)
#                            
#                            this_temp = np.nanmean(month_temp,0)
#                            
#                            site_fapar = self.flux_data.get_evi(site_data_path, current_yr, sitename)
#                            this_fapar = np.asarray(site_fapar, dtype=np.float)[month - 1]
#                            
##                            site_tmax = max(month_temp)
##                            site_tmin = min(month_temp)
##                            this_vap = np.nanmean(month_vap,0)
#                        else:
                        this_temp = today_tair
                        this_vpd = today_vpd
                                
                        
                        self.site_alpha = 1.0 # Beni wants to keep Alpha off for now
                    
                       
                        self.make_gpp = GPP_CALC()
                        # Commenting out becuase VPD is going to be a data input instead of calculated in the P_model
#                        self.make_gpp.run_grid(site_ppfd, this_month_fapar, this_temp, 
#                                               site_CO2, site_tmax, site_tmin, this_vap, 
#                                               self.site_alpha, site_elv, beta, phi_0)
                        
#                        print my_date
                        self.make_gpp.run_grid(this_ppfd, this_fapar, this_temp, site_CO2, self.site_alpha,
                                               site_elv, beta, phi_0,  absG, vpd = this_vpd)
                       
                                               
                        self.GPP_out[tNo] = self.make_gpp.gpp #  gC/m2/month
                        self.lue_out[tNo] = self.make_gpp.lue
                        self.ppfd_out[tNo] = this_ppfd
                        self.temp_out[tNo] = this_temp
                        self.vpd_out[tNo] = this_vpd
                        self.fapar_out[tNo] = this_fapar
                        self.co2_out[tNo] = site_CO2
                        self.M_out[tNo] = self.make_gpp.M
                        self.my_date[tNo] = date_frac
                        tNo += 1
                       
                       
           
   
                
    def daterange_daily(self, start_date, end_date):
        
        for n in range(int ((end_date - start_date).days)):
            cd = start_date + timedelta(n)
            if cd.month == 2 and cd.day == 29:
                self.logger.info ("leap year")
            else:
                yield cd
            
            
            
############ MAIN PROGRAM ##############

if __name__ == '__main__':
    
  

    wn = 145.0
    beta = 146.0
    phi_0 = 0.83
    absG = 1.0
    site_lat = 47.2858
    site_lon = 7.7319
    site_elv = 450
    start_yr = 2002
    end_yr = 2002
    spin_yrs = 1
    site_data_path = ('/Users/rebecca/Documents/PhD/Part2_MODEL/Code_MODEL/input/input_fluxnet_sofun/sitedata/')
    
    sitename = 'CH-Oe1'
    
    start_date_run = date(start_yr, 1,1)
    end_date_run = date(end_yr, 12, 31)
    
    base = start_date_run
    diff = end_date_run - start_date_run 
    numdays = diff.days
    date_list_daily = [base + timedelta(days = x) for x in range(0, numdays + 1)]
    
    get_gpp = RUN_PMOD_SITE(start_date_run, end_date_run)    
    get_gpp.run_pmod(sitename,  site_lat, site_lon, site_elv, start_date_run, end_date_run, site_data_path, beta, phi_0, absG, wn)
    gpp = get_gpp.GPP_out
    lue = get_gpp.lue_out
    fapar_o = get_gpp.fapar_out
    co2_out = get_gpp.co2_out
    m_out = get_gpp.M_out
    tair_out = get_gpp.temp_out
    vpd = get_gpp.vpd_out
                       
    
    
    