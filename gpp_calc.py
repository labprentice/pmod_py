# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 11:47:01 2016

@author: GreencyclesII
"""

######## Import modules#########
import os
import sys

myhome = os.path.expanduser("~")

pmod_path = os.path.join(myhome, 'GPP_mod/src') # the pmodel code location
sys.path.append(pmod_path)       


import numpy as np
from constants import c_star
from m_calc import M_CALC
from get_c13 import C13_CALC
import matplotlib.pyplot as plt


##############
# Main program
##############

class GPP_CALC:
    """
    Name: GPP_CALC
    Features: Calculates GPP for given data
    """
    def __init__(self):
            """
            Name:     GPP_CALC.__init__
            Input:    None.
            Features: Initialise class variables
                      
            """
            self.gpp = None
            self.old_frac = None
            self.gpp_old_eq = None
          
          #        ### Class Function Definitions ####
        
    def run_grid(self, ppfd, this_fapar, temp, aCO2, alpha, elv, beta, phi_0, absG, tMax = False, tMin = False, vap = False, vpd = False):
            """
            Name: GPP_CALC.run_grid
            Input: - float, ambient CO2 (aCO2) ppm
                   - float, faparn/a
                   - float, temperature (temp) C
                   - float, elevation (elv) m
                   - float, monhtly maximum daily temp (tMax) C
                   - float, monthly minimum daily tamperature (tMin) C
                   - float, vapour pressure (vap) Pa
                   - float, vapour pressure defict (vpd) Pa This is data input if running at site scale, but calcuated at global
            Outputs: - float, GPP
                    - float, C13 discrimination
            Depends: - SPLASH --> EVAP-->alpha
                     - SPLASH --> PPFD
                     
            """
            
            if tMax is not False:
              this_tMax = tMax
            else:
               this_tMax = False
               
            if tMin is not False:
              this_tMin = tMin
            else:
               this_tMin = False
               
            if vap is not False:
              this_vap = vap
            else:
               this_vap = False
               
            if vpd is not False:
              this_vpd = vpd
            else:
               this_vpd = False
               
         
           
            my_M=M_CALC()
            my_M.run_grid(temp, aCO2, elv, beta, this_tMax , this_tMin , this_vap , this_vpd) 
            self.M = my_M.m 
            self.GAMMA_ST = my_M.GSTAR
            self.K1_ = my_M.K_1
            self.ETA_ST = my_M.ESTAR
            self.vpd_Pa_ = my_M.vpd_Pa
            self.m_frac = np.sqrt(1.0 - ((c_star / self.M) ** ( 2.0 / 3.0 )))
            
            
            # GPP is zero for monhtly mean temepratures below 0C
              
            self.lue = phi_0 *\
                       self.M * \
                       self.m_frac *\
                       (alpha ** (1.0/4.0))
                       
            if type(temp) ==np.float64:
                if temp < 0.0:
                    self.lue = 0.0
            else:
                 neg_t = np.where(temp < 0.0)
                 self.lue[neg_t] = 0.0
            
            self.gpp = self.lue * \
                       absG * \
                       ppfd * \
                       this_fapar 
                     
           
                              
                                    
                       
            # 0.8 added as a multiply factor to Iabs (ppfd * fapar) becuase not all light absorbed by veg is sued for photo           
                       
            my_dC13 = C13_CALC()
            my_dC13.find_c13(aCO2, self.GAMMA_ST, self.vpd_Pa_, self.K1_, self.ETA_ST, beta)
            self.c13 = my_dC13.c13
            self.ci_ca = my_dC13.ci_ca
            self.ci_oo = my_dC13.ci_out
#            self.chi_oo_old = my_dC13.chi_out_old
#            self.ci_oo_old = my_dC13.chi_out_old * aCO2
            
#            vc_frac_old = ((self.ci_oo_old - self.GAMMA_ST) / (self.ci_oo_old + (2 * self.GAMMA_ST))) * \
#                          ((self.ci_oo_old + self.K1_) / (self.ci_oo_old - self.GAMMA_ST))
                          
#            a0 = np.sqrt(((self.K1_ ** 2) * ((self.ci_oo_old + self.K1_) / (self.ci_oo_old + (2 * self.GAMMA_ST))) ** 2) - 1)
    
#            vcmax_top_new = phi_0 * \
#                              absG * \
#                              ppfd * \
#                              this_fapar * \
#                              self.M * \
#                              (1/ np.sqrt(1 + a0 ** -2))
#                              
#            vcmax_bottom_new = (self.ci_oo - self.GAMMA_ST) /  (self.ci_oo + self.K1_)
#            
            
#            self.old_frac = ((aCO2 * self.chi_oo_old) - my_M.GSTAR)/((aCO2 * self.chi_oo_old) + (2* my_M.GSTAR))
#            self.gpp_old_eq = phi_0 * \
#                              absG * \
#                              ppfd * \
#                              this_fapar * \
#                              self.old_frac
#                              
#            self.vc_max_old = phi_0 * \
#                              absG * \
#                              ppfd * \
#                              this_fapar * \
#                              vc_frac_old
#                              
#            self.vc_max_new = vcmax_top_new / vcmax_bottom_new
######## MAIN PROGRAME ########

if __name__=='__main__':
    
    # Testing out GPP claucation with pseudo data:
    
        test_gpp = GPP_CALC()
        
        #run_grid(self, ppfd, this_fapar, temp, aCO2, alpha, elv, beta, phi_0, absG, tMax = False, tMin = False, vap = False, vpd = False):
        # All inputs are needed
        ppfd = np.float64(2000.0) #
        this_fapar = np.float64(1.0) #average
        temp = np.float64(25.0) # deg C
        alpha = np.float64(1.0) # No water limitation
        elv = np.float64(0.0) # Sea level
        beta = np.float64(146.0) # Standard beta 146.0
        phi_0 = np.float64(0.83) # Standard phi
        phi_0_low = np.float64(0.22)
        absG = np.float64(1.0) # No scaling of fapar
#        tMax = np.float64(25.0)
#        tMin = np.float64(25.0)
#        vap = np.float64(10132.5)
        vpd_in = np.float64(1000.0) #Pa
        
        co2_vals = np.linspace(0, 499, 500, endpoint = True)
        gpp_out = np.zeros(shape = 500)
        gpp_out_old = np.zeros(shape = 500)
        M_out = np.zeros(shape = 500)
        m_frac_out = np.zeros(shape = 500)
        vc_out_old = np.zeros(shape = 500)
        vc_out_new = np.zeros(shape = 500)
        mm = 0
        for aCO2 in co2_vals:
            test_gpp.run_grid(ppfd, this_fapar, temp, aCO2, alpha, elv, beta, phi_0, absG, vpd = vpd_in)
            M_out[mm] = test_gpp.M
            m_frac_out[mm] = test_gpp.m_frac
            gpp_out[mm] = test_gpp.gpp
            gpp_out_old[mm] = test_gpp.gpp_old_eq /0.83 *0.22
            vc_out_old[mm] = test_gpp.vc_max_old
            vc_out_new[mm] = test_gpp.vc_max_new

            mm += 1
        
        plt.figure(1)
        
        plt.scatter(co2_vals, gpp_out, color = 'r', label = 'new eq')
        plt.scatter(co2_vals, gpp_out_old, color = 'b', label = 'old eq')
        plt.xlabel('CO2 concentration (ppm)')
        plt.ylabel('GPP (gC/m2/month)')
        plt.title('Response of GPP to Pseudo CO2')
        plt.legend(loc = 'lower right')
        plt.xlim(-1, 500)
        
        plt.figure(2)
        
        plt.scatter(co2_vals, vc_out_new, color = 'r', label = 'new eq')
        plt.scatter(co2_vals, vc_out_old, color = 'b', label = 'old eq')